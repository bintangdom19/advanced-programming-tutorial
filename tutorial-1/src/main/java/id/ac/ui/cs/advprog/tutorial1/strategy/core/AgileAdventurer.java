package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    public AgileAdventurer() {
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias() {
        return "Agile";
    }
    //ToDo: Complete me
}
