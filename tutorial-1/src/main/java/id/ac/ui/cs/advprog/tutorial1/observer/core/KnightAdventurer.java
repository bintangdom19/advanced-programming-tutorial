package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        @Override
        public void update() {
                String questType = guild.getQuestType();
                Quest quest = guild.getQuest();
                this.getQuests().add(quest);
        }

        //ToDo: Complete Me
}
