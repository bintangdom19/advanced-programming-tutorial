package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                //ToDo: Complete Me
                agileAdventurer = new AgileAdventurer(guild);
                knightAdventurer = new KnightAdventurer(guild);
                mysticAdventurer = new MysticAdventurer(guild);
        }

        @Override
        public void addQuest(Quest quest) {
                Quest currentQuest = questRepository.save(quest);
                if(currentQuest != null) {
                        guild.addQuest(currentQuest);
                }
        }

        @Override
        public List<Adventurer> getAdventurers() {
                return guild.getAdventurers();
        }

        //ToDo: Complete Me
}
