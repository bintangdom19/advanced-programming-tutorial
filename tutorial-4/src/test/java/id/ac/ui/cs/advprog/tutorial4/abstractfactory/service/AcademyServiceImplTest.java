package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    private AcademyRepository academyRepository;

    private AcademyService academyService;

    // TODO create tests

    @BeforeEach
    public void setUp() {
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void testGetKnightAcademies() {
        assertEquals(2, academyService.getKnightAcademies().size());
    }

    @Test
    public void testProduceKnight() {
        academyService.produceKnight("Lordran", "majestic");
        assertNotNull(academyService.getKnight());
        assertEquals("Majestic Knight", academyService.getKnight().getName());
    }
}
