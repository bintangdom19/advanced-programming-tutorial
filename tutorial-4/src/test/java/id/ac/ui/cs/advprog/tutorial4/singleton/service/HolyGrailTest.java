package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests

    @InjectMocks
    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp() {
        holyGrail = mock(HolyGrail.class);
    }

    @Test
    public void whenMakeAWishIsCalledItShouldSetAWish() {
        holyGrail.makeAWish("TestWish");
        verify(holyGrail).makeAWish("TestWish");
    }

    @Test
    public void whenGetHolyWishIsCalledItShouldReturnHolyWishInstance() {
        HolyWish holyWish = holyGrail.getHolyWish();
        lenient().when(holyGrail.getHolyWish()).thenReturn(holyWish);
    }
}
