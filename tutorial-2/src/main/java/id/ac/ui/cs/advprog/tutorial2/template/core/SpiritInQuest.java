package id.ac.ui.cs.advprog.tutorial2.template.core;

import java.util.ArrayList;
import java.util.List;

public abstract class SpiritInQuest {

    public List attackPattern() {
        List<String> list = new ArrayList<>();
        // TODO: Complete Me
        list.add(summon());
        list.add(getReady());
        list.add(attackWithBuster());
        list.add(attackWithQuick());
        list.add(attackWithArts());
        list.add(attackWithSpecialSkill());

        if(this.getClass().getSimpleName().equals("Archer")) {
            list.add(2, buff());
        } if(this.getClass().getSimpleName().equals("Lancer")) {
            list.add(4, buff());
        } if(this.getClass().getSimpleName().equals("Saber")) {
            list.add(3, buff());
        }
        return list;
    }


    public String summon() {
        return "Summon a Spirit...";
    }

    public String getReady() {
        return "Spirit ready to enter the Quest";
    }

    protected abstract String buff();

    protected abstract String attackWithBuster();

    protected abstract String attackWithQuick();

    protected abstract String attackWithArts();

    protected abstract String attackWithSpecialSkill();
}
